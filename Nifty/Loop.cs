﻿using Nifty.Loops;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nifty
{
    public static class Loop
    {
        public delegate void Advance<T>(ref T value);

        private static IEnumerable<T> ForImpl<T>(Func<T> setup, Func<T, bool> test, Advance<T> adv)
        {
            for (T i = setup(); test(i); adv(ref i))
                yield return i;
        }

        public static EnumerableWrap<T> For<T>(Func<T> setup, Func<T, bool> test, Advance<T> adv)
        {
            return new EnumerableWrap<T>(ForImpl(setup, test, adv));
        }

        public static EnumerableWrap<T> For<T>(T start, Func<T, bool> test, Advance<T> adv)
        {
            return For(() => start, test, adv);
        }

        private static IEnumerable<T> ForImpl<T>(Func<T> setup, Func<T, bool> test, Func<T, T> adv)
        {
            for (var i = setup(); test(i); i = adv(i))
                yield return i;
        }
        
        public static EnumerableWrap<T> For<T>(Func<T> setup, Func<T, bool> test, Func<T, T> adv)
        {
            return new EnumerableWrap<T>(ForImpl(setup, test, adv));
        }

        public static EnumerableWrap<T> For<T>(T start, Func<T, bool> test, Func<T, T> adv)
        {
            return For(() => start, test, adv);
        }

        public static WhileLoop While(Func<LoopRunContext, bool> test)
        {
            return new WhileLoop(test);
        }

        public static YieldBuilder<T> Yield<T>(Func<T> generator)
        {
            return new YieldBuilder<T>(generator);
        }
    }
}
