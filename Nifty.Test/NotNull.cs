﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using NUnit.Framework;

namespace Nifty.Tests
{
    public class NotNull
    {
        [Test]
        public void CorrectlyConvertsToString()
        {
            NotNull<string> name = "Tosh";

            string converted = name;

            Assert.AreEqual("Tosh", converted);
        }

        [Test]
        public void ValuePropertyCorrectlyReturnsValue()
        {
            NotNull<string> name = "Jeff";

            Assert.AreEqual("Jeff", name.Value);
        }

        [Test]
        public void ThrowsAtCallSite_WithNullValue()
        {
            string message = "hi";
            Action<NotNull<string>> print = s => message = s;

            try
            {
                string input = null;
                print(input);
            }
            catch (NullValueException)
            { }

            Assert.AreEqual("hi", message);
        }

        [Test]
        public void EqualityOperatorWorks_WithObjectOnRight()
        {
            NotNull<string> name = "Bill";

            Assert.IsTrue(name == "Bill");
        }

        [Test]
        public void EqualityOperatorWorks_WithObjectOnLeft()
        {
            NotNull<string> name = "Bill";

            Assert.IsTrue("Bill" == name);
        }

        [Test]
        public void EqualityOperatorWorks_WithNotNullOnEachSide()
        {
            Func<NotNull<string>, NotNull<string>, bool> equal
                = (lhs, rhs) => lhs == rhs;

            Assert.IsFalse(equal("Fish", "fish"));
        }

        [Test]
        public void InequalityOperatorWorks_WithObjectOnRight()
        {
            NotNull<string> name = "Bill";

            Assert.IsTrue(name != "bill");
        }

        [Test]
        public void InequalityOperatorWorks_WithObjectOnLeft()
        {
            NotNull<string> name = "Bill";

            Assert.IsTrue("Ralph" != name);
        }

        [Test]
        public void InequalityOperatorWorks_WithNotNullOnEachSide()
        {
            Func<NotNull<string>, NotNull<string>, bool> notEqual
                = (lhs, rhs) => lhs != rhs;

            Assert.IsFalse(notEqual("fish", "fish"));
        }

        [Test]
        [ExpectedException(typeof(NullValueException))]
        public void ValueThrowsIfDefaultConstructorUsed()
        {
            var n = new NotNull<string>();

            string s = n.Value;
        }

        [Test]
        [ExpectedException(typeof(NullValueException))]
        public void ConversionThrowsIfDefaultConstructorUsed()
        {
            var n = new NotNull<string>();

            string s = n;
        }

        [Test]
        [ExpectedException(typeof(NullValueException))]
        public void EqualityOperatorThrowsIfDefaultConstructorIsUsedOnLeft()
        {
            string s = null;

            Assert.IsTrue(s == new NotNull<string>());
        }

        [Test]
        [ExpectedException(typeof(NullValueException))]
        public void EqualityOperatorThrowsIfDefaultConstructorIsUsedOnRight()
        {
            string s = null;

            Assert.IsTrue(new NotNull<string>() == s);
        }

        [Test]
        [ExpectedException(typeof(NullValueException))]
        public void InequalityOperatorThrowsIfDefaultConstructorIsUsedOnLeft()
        {
            string s = null;

            Assert.IsFalse(s != new NotNull<string>());
        }

        [Test]
        [ExpectedException(typeof(NullValueException))]
        public void InequalityOperatorThrowsIfDefaultConstructorIsUsedOnRight()
        {
            string s = null;

            Assert.IsFalse(new NotNull<string>() != s);
        }

        [Test]
        public void ObjectEqualsTestsCorrectlyForEqual()
        {
            NotNull<string> name = "Terrance";
            object o = "Terrance";

            Assert.IsTrue(name.Equals(o));
        }

        [Test]
        public void ObjectEqualsTestsCorrectlyForNotEqual()
        {
            NotNull<string> name = "Terrance";
            object o = "Felicity";

            Assert.IsFalse(name.Equals(o));
        }

        [Test]
        public void GetHashCodeReturnsCorrectly()
        {
            NotNull<string> name = "fishbob";

            Assert.AreEqual(name.GetHashCode(), "fishbob".GetHashCode());
        }

        [Test]
        [ExpectedException(typeof(NullValueException))]
        public void GetHashCodeThrowIfNull()
        {
            var v = new NotNull<string>();

            v.GetHashCode();
        }

        class OL
        {
            public bool StringCalled;
            public bool ObjectCalled;

            public void Go(string param)
            {
                StringCalled = true;
            }

            public void Go(object param)
            {
                ObjectCalled = true;
            }
        }

        [Test]
        public void OverloadResolutionWorks()
        {
            var ol = new OL();

            NotNull<string> name = "Enrique";

            ol.Go(name);

            Assert.IsFalse(ol.ObjectCalled);
            Assert.IsTrue(ol.StringCalled);
        }

        [Test]
        public void Interface_works()
        {
            IEnumerable<char> greeting = "hello there";

            NotNull<IEnumerable<char>> chars = Not.Null(greeting);
        }

        [Test]
        public void Func_works()
        {
            NotNull<Func<string>> getString = Not.Null<Func<string>>(() => "hi");

            Assert.AreEqual("hi", getString.Value());
        }

        [Test]
        public void Expression_works()
        {
            NotNull<Expression<Func<int>>> getInt = Not.NullExpr<Func<int>>(() => 5);

            Assert.AreEqual(5, getInt.Value.Compile()());
        }

        [Test]
        public void ForLoop_dogfood()
        {
            NotNull<IEnumerable<char>> chars = Loop.For('a', c => c <= 'z', c => ++c);
            var list = chars.Value.ToList();

            Assert.AreEqual(26, list.Count);
        }
    }
}
