﻿using NUnit.Framework;
using System;
using System.Diagnostics;
using System.Text;

namespace Nifty.Tests
{
    public class Wrap
    {
        [Test]
        public void WrapUp_wraps_void()
        {
            var output = new StringBuilder();
            Action<object> log = s => output.Append(s.ToString());

            var wrapped = Nifty.Wrap.Up(action =>
            {
                log(1);
                try
                {
                    action();
                }
                catch (Exception)
                {
                    log(3);
                }
                finally
                {
                    log(4);
                }
            });

            wrapped.Do(() =>
            {
                log(2);
                int d = 0;
                log(2 / d);
            });

            Assert.AreEqual("1234", output.ToString());
        }

        [Test]
        public void WrapUp_wraps_value_expr()
        {
            var output = new StringBuilder();
            var wrapped = Nifty.Wrap.Up<double>(f =>
                {
                    try
                    {
                        return f();
                    }
                    catch (Exception e)
                    {
                        output.AppendLine(e.Message);
                        return -1;
                    }
                });

            var n = 1;
            var d = 0;
            var r = wrapped.Do(() => n / d);

            Assert.AreEqual(-1, r);
            Assert.IsFalse(String.IsNullOrEmpty(output.ToString()));
        }
    }
}
